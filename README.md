# Inleiding GIT - ITF

- Inleidend filmpje: [Learn to Git: Basic Concepts](https://www.youtube.com/watch?v=8KCQe9Pm1kg)
- Cursustekst [Inleiding_Github.pdf](http://jkwadraat.sinners.be/git/Inleiding_Github.pdf) (met handige cheat sheet als appendix)

![Samenwerken met GIT](assets/samenwerken.png "Samenwerken met GIT")

### Basisvereisten (eenmalig te installeren)

Volgende tools hoef je maar eenmalig te installeren:

- De hoogste versie van [**Node**](https://nodejs.org/en/) (optioneel)
- [**Git**](https://git-scm.com/)

Tip: je kan vanuit Git Bash switchen naar de standaard cmd-prompt (`cmd`) en terug (`bash`) of gebruik ze samen met [cmder](http://cmder.net/)

### GitHub-account aanmaken

1. Maak met je TM-mailadres een GitHub-account aan op https://github.com
2. Vraag een upgrade van je account via https://education.github.com/

### Instellen van je identiteit (en default editor) 

```bash
git config --global user.name "Name"
git config --global user.email "name@thomasmore.be"
git config --global user.editor notepad
(git config --global core.editor "'c:/program files/sublime text 3/subl.exe' -w")
```

Default editor wordt bijvoorbeeld geopend bij: `git config --edit`

# Een solo-project starten op GitHub

### Opzetten van een lokale repo

1. Open **Git Bash** in je projectmap
2. Initialiseer een nieuwe Git-repo `git init`
3. Maak de bestanden **README.md** en **.gitignore** aan   
(GitHub markdown: https://guides.github.com/features/mastering-markdown/)
4. Voeg alle bestanden (uitgezonderd deze die zijn uitgesloten via **.gitignore**) uit deze map aan de staging area toe   
`git add .`
5. Verplaats de bestanden van de staging area naar de lokale repo `git commit -m "First commit"`

### Opzetten van een remote repo

1. Maak een nieuwe, lege, (private) repo **projectNaam** op GitHub   
(dus zonder ***.gitignore*** en zonder ***README***!)
2. Verbind de lokale repo met de remote repo `git remote add <remote‐name> <remote‐url>`   
bv: `git remote add origin https://github.com/teamleader/projectNaam.git`
3. Push de lokale repo naar de remote repo `git push ‐u origin master`

### Stagen en committen van lokaal gewijzigde bestanden

1. Controleer de status `git status`
2. Voeg de gewenste (of alle) wijzigingen aan de staging area toe `git add <file-name>` (of `git add .` of `git add --all`)   
Verschil? https://gist.github.com/dsernst/ee240ae8cac2c98e7d5d
3. Verplaats de bestanden van de staging area naar de lokale repo `git commit -m "Comment"`
4. Push de lokale repo naar de remote repo `git push`

# Een team-project starten op GitHub

Werk je met twee of meer teamleden aan hetzelfde project, dan volg je best deze werkwijze. Begin alvast met een teamleader 🎓 aan te duiden en een naam voor het project te kiezen.

### Opzetten van een lokale repo door de teamleader 🎓

**De overige teamleden 👤👤 doen voorlopig nog niets!**

1. Open **Git Bash** in je projectmap
2. Initialiseer een nieuwe Git-repo `git init`
3. Maak de bestanden **README.md** en **.gitignore** aan
4. Voeg alle bestanden (uitgezonderd deze die zijn uitgesloten via **.gitignore**) uit deze map aan de staging area toe   
`git add .`
5. Verplaats de bestanden van de staging area naar de lokale repo `git commit -m "First commit"`

### Opzetten van een remote repo door de teamleader 🎓

1. Maak een nieuwe, lege, (private) repo **projectNaam** op GitHub   
(dus zonder ***.gitignore*** en zonder ***README***!)
2. Verbind de lokale repo met de remote repo `git remote add <remote‐name> <remote‐url>`   
bv: `git remote add origin https://github.com/teamleader/projectNaam.git`
3. Push de lokale repo naar de remote repo `git push ‐u origin master`

### Teamleader 🎓 nodigt andere teamleden 👤👤 uit voor samenwerking

1. Klik op de pagina **https://github.com/teamleader/projectNaam** op ***settings***
2. Klik links in het menu op ***Collaborators***
3. Nodig overige teamleden 👤👤 uit
4. Teamleden aanvaarden de uitnodiging via een e-mail van GitHub

### Teamleden 👤👤 clonen van een remote repo

1. Elk teamlid maakt een exacte copy van de remote repo `git clone https://github.com/teamleader/projectNaam.git`

Proficiat! De samenwerking kan beginnen.

### Stagen en committen van lokaal gewijzigde bestanden 🎓👤👤

Maak regelmatig back-ups (push) zodat de ander teamleden weten waar je mee bezig bent.   
Wacht dus niet te lang met wijzigingen op GitHub te plaatsen. 

1. Controleer de status `git status`
2. Voeg de gewenste (of alle) wijzigingen aan de staging area toe `git add <file-name>` (of `git add .` of `git add --all`)
3. Verplaats de bestanden van de staging area naar de lokale repo `git commit -m "Comment"`
4. Synchroniseer met de remote repo `git pull --rebase` (en los eventueel de **conflicten** * op)
5. Push de lokale repo naar de remote repo `git push`

# Conflicten oplossen *

Conflicten ontstaan indien beide programmeurs (op dezelfde plaats!) in hetzelfde bestand wijzigingen aanbrengen.

![Conflict in GIT](assets/conflict.png "Git workflow")

1. Verwijder de markers **<<<<<<<**, **=======** en **>>>>>>>** en past de code aan die je wil behouden in de definitieve versie
2. Voeg het aangepast bestand aan de staging area toe `git add <conflicted-file>`
3. Vervolgens geeft je met `git rebase ‐‐continue` aan dat GIT de rebase-actie verder kan voltooien

Herhaal deze drie stappen tot alle conflicten zijn opgelost.

# Workflow

![GIT workflow](assets/workflow.png "GIT werkflow")

# Gist

- Gist is meer bedoeld voor codefragmenten. Ook gists hebben een complete revisiegeschiedenis.
- Bijvoorbeeld: https://gist.github.com/pverhaert/


# GitHub Pages

- Host je website op GitHub.
- https://pages.github.com/
- Bijvoorbeeld: https://pverhaert.github.io/

# GitBook

- Host je documentatie of cursus op GitHub
- https://www.gitbook.com/
- Voorbeeld: https://1itf.gitbook.io/jquery/
